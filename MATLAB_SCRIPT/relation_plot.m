clearvars;

filename='max_hmmer';

fid=fopen(filename,'r');

D=[];
k0=[];
k1=[];
k2=[];
F=[];
M=[];
HW=[];
IPC=[];
order=[];

for i=1:100
    line = fgets(fid);
    if(~ischar(line))
        break;
    end
%     fprintf('%s\n',line);
    set = textscan(line,'D: %f k0: %f, k1: %f, k2: %f, F: %f, M: %f IPC: %f');
    D=[D;set{1}];
    k0=[k0;set{2}];
    k1=[k1;set{3}];
    k2=[k2;set{4}];
    F=[F;set{5}];
    M=[M;set{6}];
    tmp=(set{2}+set{3}+set{4})*(1+set{6}+set{1}*set{6});
    HW=[HW;tmp];
    IPC=[IPC;set{7}];
    order=[order;i];
end

figure;
% title('relation between IPC and parameters for gcc trace file');

subplot(3,3,1);

% text(250,4,'relation between IPC and parameters for gcc trace file','HorizontalAlignment','center','VerticalAlignment','top');

fig1=scatter(HW,IPC,'fill','markerfacecolor',[1,0,0]);

grid on;
xlabel('HW=total # of FUs + SQ size + DQ size');
ylabel('IPC');
title('Fig7-1 IPC vs HW #');

subplot(3,3,2);

fig2 = scatter(k0,IPC);
grid on;

xlabel('k0:# of type-0 FU');
ylabel('IPC');
title('Fig7-2 IPC vs k0');

subplot(3,3,3);
fig3 = scatter(k1,IPC,'d','markerfacecolor',[0,1,0]);


xlabel('k1:# of type-1 FU');
ylabel('IPC');
title('Fig7-3 IPC vs k1');
grid on;


subplot(3,3,4);
fig4 = scatter(k2,IPC,'d','markerfacecolor',[0.8,0.8,0]);
xlabel('k2:# of type-2 FU');
ylabel('IPC');
title('Fig7-4 IPC vs k1');
grid on;

subplot(3,3,5);
hist([k0,k1,k2]);
xlabel('blue: TYPE-0; green: TYPE-1; brown: TYPE-2');
ylabel('number in top 100');
title('Fig7-5 histogram of the best 100 IPC parameters against k0,k1 and k2');


subplot(3,3,6);
fig6=scatter(D,IPC,'d','markerfacecolor',[0,1,0]);
grid on;
xlabel('d:DQ multiplier');
ylabel('IPC');
title('Fig7-6 IPC vs d');

subplot(3,3,7);
fig6=scatter(F,IPC,'d','markerfacecolor',[0,0,0]);
grid on;
xlabel('f:fetch rate');
ylabel('IPC');
title('Fig7-7 IPC vs f');

subplot(3,3,8);
fig6=scatter(M,IPC,'d','markerfacecolor',[1,0,0]);
grid on;
xlabel('d:SQ multiplier');
ylabel('IPC');
title('Fig7-8 IPC vs m');

subplot(3,3,9);
hist([D,F,M]);

xlabel('blue: d; green: f; brown: m');
ylabel('number in top 100');
title('Fig7-9 histogram of the best 100 IPC parameters against d,f and m');



