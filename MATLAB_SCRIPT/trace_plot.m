clearvars;

filename='gcc.100k.trace';
fid=fopen(filename,'r');

op_gcc=[];

while(true)
    line = fgets(fid);
    if(~ischar(line))
        break;
    end
%     fprintf('%s\n',line);
    set = textscan(line,'%s %f %f %f %f');
%     fprintf('%f\n',set{2});
    op_gcc=[op_gcc;set{2}];
end



filename='gobmk.100k.trace';
fid=fopen(filename,'r');

op_gobmk=[];

while(true)
    line = fgets(fid);
    if(~ischar(line))
        break;
    end
%     fprintf('%s\n',line);
    set = textscan(line,'%s %f %f %f %f');
%     fprintf('%f\n',set{2});
    op_gobmk=[op_gobmk;set{2}];
end



filename='mcf.100k.trace';
fid=fopen(filename,'r');

op_mcf=[];

while(true)
    line = fgets(fid);
    if(~ischar(line))
        break;
    end
%     fprintf('%s\n',line);
    set = textscan(line,'%s %f %f %f %f');
%     fprintf('%f\n',set{2});
    op_mcf=[op_mcf;set{2}];
end



filename='hmmer.100k.trace';
fid=fopen(filename,'r');

op_hmmer=[];

while(true)
    line = fgets(fid);
    if(~ischar(line))
        break;
    end
%     fprintf('%s\n',line);
    set = textscan(line,'%s %f %f %f %f');
%     fprintf('%f\n',set{2});
    op_hmmer=[op_hmmer;set{2}];
end


hist([op_gcc,op_gobmk,op_mcf,op_hmmer]);