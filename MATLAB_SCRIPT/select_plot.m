clearvars;

filename='max_hmmer';

fid=fopen(filename,'r');

D=[];
k0=[];
k1=[];
k2=[];
F=[];
M=[];
HW=[];
IPC=[];
order=[];

for i=1:100
    line = fgets(fid);
    if(~ischar(line))
        break;
    end
%     fprintf('%s\n',line);
    set = textscan(line,'D: %f k0: %f, k1: %f, k2: %f, F: %f, M: %f IPC: %f');
    tmp=(set{2}+set{3}+set{4})*(1+set{6}+set{1}*set{6});
    
%     if( and( tmp < 250, set{7} > 2.6) )
    if( set{7} > 2.5)
        fprintf('HW:%d ',tmp);
        fprintf('%s',line);        
        D=[D;set{1}];
        k0=[k0;set{2}];
        k1=[k1;set{3}];
        k2=[k2;set{4}];
        F=[F;set{5}];
        M=[M;set{6}];

        HW=[HW;tmp];
        IPC=[IPC;set{7}];
        order=[order;i];
    
    end
end

subplot(3,1,1);
scatter(HW,IPC);
xlabel('HW=total # of FUs + SQ size + DQ size');
ylabel('IPC');
title('Fig8-1 IPC vs HW #');



subplot(3,1,2);
hist([k0,k1,k2]);
xlabel('blue: TYPE-0; green: TYPE-1; brown: TYPE-2');
ylabel('number in top');
title('Fig8-2 histogram of the most economic parameters against k0,k1 and k2');


subplot(3,1,3);
hist([D,F,M]);
xlabel('blue: d; green: f; brown: m');
ylabel('number in top');
title('Fig8-3 histogram of the most economic parameters against d,f and m');
