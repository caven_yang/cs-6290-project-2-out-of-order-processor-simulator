#ifndef PROCSIM_HPP
#define PROCSIM_HPP

#include <cmath>
#include <cinttypes>
#include <cstdint>
#include <cstdio>
//#include <cstdlib>
#include <inttypes.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <unistd.h>

using namespace std;

#define DEFAULT_K0 1
#define DEFAULT_K1 2
#define DEFAULT_K2 3

#define DEFAULT_M 2
#define DEFAULT_F 4
#define DEFAULT_D 4

#define FU_TYPE 3
#define INIT_TAG 0
#define REG_TOT 32

#define NOT_USED -3//used in op_code only!
#define FIRED -5 //used in op_code in SQ only!
#define NON_TAG 0 //used in entry3
#define NOP -1 // used as op_code or src/dest reg
#define NON_REG 129//used in SB

#define IF_BUF_LEN 20
#define RETIRE_SIZE 40//size of the retired buffer; 

//this is to have the result printed out in order

const uint16_t LAT_FU[] = {1,2,3};

typedef struct _proc_inst_t
{
  uint32_t instruction_address;
  int32_t op_code;
  int32_t src_reg[2];
  int32_t dest_reg;
  bool src_ready[2];
  bool dest_ready;//in DQ this is marked if the instr. is ready to move to SQ;in SQ this is marked if the instr. is ready to fire
  uint64_t dest_tag;
  uint64_t src_tag[2];
    // You may introduce other fields as needed
  uint64_t FETCH,DISP,SCHED,EXEC,STATE;
  bool SCHED_RDY,FIRE_RDY,FIRE_WAIT,COMPLETED;
  _proc_inst_t();
  bool printOutPlain();
  bool printOut();
  bool reset();
} proc_inst_t;

typedef struct _proc_stats_t
{
  float avg_inst_fire;
  unsigned long retired_instruction;
  unsigned long cycle_count;
  float avg_ipc;
  float perc_branch_pred;  
} proc_stats_t;

struct my_clock_t{
  uint64_t cycle;
  bool inHalf1;
  my_clock_t();
  bool tick();
};

bool read_instruction(proc_inst_t* p_inst);

void setup_proc(uint64_t d, uint64_t k0, uint64_t k1, uint64_t k2, uint64_t f, uint64_t m);
void run_proc(proc_stats_t* p_stats);
void complete_proc(proc_stats_t* p_stats);

struct entry3{ //for CDB,RF and SB
  uint16_t reg_num;//not used in RF
  bool ready;
  uint64_t tag;
  bool negative;//used in SB
  entry3(uint16_t num, bool rdy, uint64_t tagging);
  bool trans(const proc_inst_t &src);
};

struct IF_t{
  vector<proc_inst_t> Q;
  bool init(uint32_t len);
  bool print();
  bool fetchInstr();//return false on completing fetching all instructions;
};

struct DISP_t{
  vector<proc_inst_t> Q;
  bool init(uint32_t len);
  bool print();
  bool loadFromIF();
  bool reserveSQ();
  bool reserveSQdbg();
  bool visitRF();//prepare the instr. in DQ so that they agree w/ RF
  bool printOut(); //standard printout for the 
  bool display(); //for proc_t.screen only!
  bool displayAll(); //display whatever is in proc_t.screen in order
};

struct SCHED_t{
  vector<proc_inst_t> Q;
  bool init(uint32_t len);
  bool print(uint16_t FU);
};

struct RF_t{
  vector<entry3> Q;
  bool init();
  bool print();
};

struct CDB_t{
  vector<entry3> Q;
  bool print();
};

struct SB_t{
  vector<vector<entry3> > Q;
  bool init(uint16_t LAT,uint32_t FU_NUM);
  bool print(uint16_t FU);
};


struct proc_t{
  uint16_t fetch_rate;
  my_clock_t clock;
  uint64_t avail_tag;
  bool allFetched,allDispatched;
  IF_t IF; //IF buffer
  DISP_t DQ;//dispatch queue
  SCHED_t SQ[FU_TYPE]; //schedule queue
  RF_t RF;
  CDB_t CDB;
  SB_t SB[FU_TYPE];
  DISP_t retired;
  DISP_t screen;
  bool printAll();
  uint64_t genTag();
  bool SQmarkFire();
  bool execFU();
  bool bypassSQ(const entry3& result);
  bool updateSQbyCDB();
  bool stateUpdate();
  bool mvDQtoSQ();//actually move an instr from SQ to the correct SQ posistion!
  bool fireSQ();//fire the instr. in the SQ that are ready!
  bool completeExecFU();
  bool RFwrtByCDB();
  bool RFwrtByCDBdbg();
  bool DQvisitRF();
  bool SQdltCompleted();
  bool printRetired();  
};


bool read_instruction(proc_inst_t* p_inst);

uint16_t numVacancyQ(const vector<proc_inst_t> &que);
uint16_t firstVacancy(const vector<proc_inst_t> &que,bool &has);
uint16_t findMinIndex(const vector<uint64_t> &tags); //find the smallest tag indexed by the vector!don't op on am empty vector

uint16_t findQIndex(const vector<entry3> & entries,uint64_t tagging);//return entries.size()+1 if the tagging is not found
uint16_t findQIndex(const vector<proc_inst_t>& entries,uint64_t tagging);



extern proc_t model;
extern proc_stats_t stats;


#endif /* PROCSIM_HPP */
/*

  while(true){
    //printf("for cycle %lu??????????????\n", model.clock.cycle);
    
   
    */
