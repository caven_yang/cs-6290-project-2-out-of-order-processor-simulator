#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

using namespace std;

typedef unsigned int uint;

struct result_t{
  int f,d,m,j,k,l;
  float IPC;
};


uint LEN = 512;

#define MIN_NUM 100

int main(){
  result_t inst;
  vector<result_t> min;
  scanf("\n");
  bool in_bt;
  
  for(uint j=0; ; j++){
    if(scanf("D: %d k0: %d k1: %d k2: %d F: %d M: %d Avg inst fired per cycle:! %f\n",&inst.d,&inst.j,&inst.k, &inst.l, &inst.f, &inst.m,&inst.IPC) == 7){
      //printf("D: %d k0: %d, k1: %d, k2: %d, F: %d, M: %d IPC: %f\n",inst.d, inst.j, inst.k, inst.l, inst.f, inst.m, inst.IPC);
      in_bt = false;
      for(uint i=0; i<min.size(); i++){
	if( min[i].IPC < inst.IPC){
	  min.insert(min.begin()+i, inst);
	  if( min.size() >= MIN_NUM)
	    min.pop_back();
	  in_bt = true;
	  break;
	}
      }

      
      if( !in_bt && min.size() < MIN_NUM)
	min.push_back(inst);


    }
      
    else
      break;
    
  }
  

  for(uint i=0; i<min.size(); i++){
    inst = min[i];
    printf("D: %d k0: %d, k1: %d, k2: %d, F: %d, M: %d IPC: %f\n",inst.d, inst.j, inst.k, inst.l, inst.f, inst.m, inst.IPC);
  }

}
