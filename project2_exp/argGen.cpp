#include <iostream>
#include <stdio.h>

typedef unsigned int uint;

uint  FU_VARY = 3;
uint SQ_VARY = 3;
uint DQ_VARY = 3;
uint FETCH_VARY = 3;

uint FU_OP[] = {1,2,3};//length of this array = FU_VARY: k0,k1,k2
uint SQ_OP[] = {2,4,8};//length of this array = SQ_VARY: m
uint DQ_OP[] = {1,2,4};//length of this array = DQ_VARY: d
uint FETCH_OP[] = {2,4,8};//length of this array = FETCH_VARY:f

int main(){
  uint f,d,m,j,k,l;

  for(uint FU_1 = 0; FU_1 < FU_VARY; FU_1++){
    j = FU_OP[FU_1];
    for( uint FU_2 = 0; FU_2 < FU_VARY; FU_2 ++){
      k = FU_OP[FU_2];
      for( uint FU_3 = 0; FU_3 < FU_VARY; FU_3 ++){
	l = FU_OP[FU_3];
	for(uint SQ = 0; SQ < SQ_VARY; SQ++){
	  m = SQ_OP[SQ];
	  for(uint DQ = 0; DQ < DQ_VARY; DQ++){
	    d = DQ_OP[DQ];
	    for(uint FET = 0; FET < FETCH_VARY; FET++){
	      f = FETCH_OP[FET];
	      printf("-j %u -k %u -l %u -m %u -d %u -f %u\n", j,k,l,m,d,f);
	    }
	  }
	}
      }
    }
  }



  return 0;
}
