#include "procsim.hpp"
extern proc_t model;
extern proc_stats_t stats;
/**
 * Subroutine for initializing the processor. You many add and initialize any global or heap
 * variables as needed.
 * XXX: You're responsible for completing this routine
 *
 * @k0 Number of k0 FUs
 * @k1 Number of k1 FUs
 * @k2 Number of k2 FUs
 * @f Number of instructions to fetch
 * @m Schedule queue multiplier
 */
void setup_proc(uint64_t d,uint64_t k0, uint64_t k1, uint64_t k2, uint64_t f, uint64_t m) {
  uint32_t LEN_DISP = d*m*(k0+k1+k2);
  uint32_t LEN_SCHED[FU_TYPE];
  model.fetch_rate = f;
  LEN_SCHED[0] = m*k0;
  LEN_SCHED[1] = m*k1;
  LEN_SCHED[2] = m*k2;
  //uint32_t IF_BUF_LEN = LEN_DISP;
  model.avail_tag = INIT_TAG;
  model.allFetched = model.allDispatched = false;
  (model.IF).Q.clear();
  (model.DQ).init(LEN_DISP);
  (model.RF).init();//reset according to REG_TOT
  for(uint16_t i=0; i<FU_TYPE; i++){
    (model.SQ[i]).init(LEN_SCHED[i]);
  }
  (model.SB[0]).init(LAT_FU[0],k0);
  (model.SB[1]).init(LAT_FU[1],k1);
  (model.SB[2]).init(LAT_FU[2],k2);
  //printf("INST\tFETCH\tDISP\tSCHED\tEXEC\tSTATE\n");
  
}

/**
 * Subroutine that simulates the processor.
 *   The processor should fetch instructions as appropriate, until all instructions have executed
 * XXX: You're responsible for completing this routine
 *
 * @p_stats Pointer to the statistics structure
 */
void run_proc(proc_stats_t* p_stats) {
  bool PRINT = !true;
  
  while(true){
    //the first half
    if(PRINT){
      printf("*********************at the start of cycle %" PRIu64 "\n", model.clock.cycle);
      model.printAll();
    }
    model.RFwrtByCDB();//state update
    model.fireSQ();
    model.execFU();    
    model.mvDQtoSQ();
    (model.DQ).loadFromIF();
    
    (model.IF).fetchInstr();    
    //    model.printAll();
    
    
    model.SQmarkFire();
    model.DQ.reserveSQ();
    model.completeExecFU();
    model.clock.tick();
    //the second half
    model.DQvisitRF();    
    model.updateSQbyCDB();
    model.SQdltCompleted();
    model.clock.tick();
    model.retired.printOut();
    //model.screen.display();

    if(stats.retired_instruction > 99999)
    //if(model.clock.cycle > 1000)
      break;
    
    

  }
}

/**
 * Subroutine for cleaning up any outstanding instructions and calculating overall statistics
 * such as average IPC or branch prediction percentage
 * XXX: You're responsible for completing this routine
 *
 * @p_stats Pointer to the statistics structure
 */
void complete_proc(proc_stats_t *p_stats) {
  //model.screen.displayAll();
  //printf("\n");
  p_stats->cycle_count = model.clock.cycle;
  p_stats->retired_instruction = stats.retired_instruction;
  p_stats->avg_inst_fire = float(p_stats->retired_instruction)/float(p_stats->cycle_count);
}



////////////////_proc_inst_t/////////////
_proc_inst_t::_proc_inst_t(){
  instruction_address = 0;
  op_code = NOT_USED;
  src_reg[0] = src_reg[1] = dest_reg = NOP;
  dest_ready = src_ready[0] = src_ready[1] = false;
  src_tag[0] = src_tag[1] = dest_tag = 0;
  FETCH = DISP = SCHED = EXEC = STATE = 0;
  SCHED_RDY = FIRE_RDY = FIRE_WAIT = COMPLETED = false;
}

bool _proc_inst_t::printOutPlain(){
  if( op_code == NOT_USED)
    return false;

  //printf("%lu\t%lu\t%lu\t%lu\t%lu\t%lu\n",dest_tag,FETCH,DISP,SCHED,EXEC,STATE);
 
  return true;

}


bool _proc_inst_t::printOut(){
  if( op_code == NOT_USED)
    return false;

  //printf("%lu\t%lu\t%lu\t%lu\t%lu\t%lu\t",dest_tag,FETCH,DISP,SCHED,EXEC,STATE);
  
  printf("%u/%d//%d/%d/%d//",instruction_address,op_code,dest_reg,src_reg[0],src_reg[1]);
  if(dest_ready)
    printf("rdy/");
  else
    printf("not/");
  
  if(src_ready[0])
    printf("rdy/");
  else
    printf("not/");

  if(src_ready[1])
    printf("rdy/");
  else
    printf("not/");

  //  printf("/%lu/%lu/%lu//",dest_tag,src_tag[0],src_tag[1]);
  
  if(!COMPLETED){
    if(SCHED_RDY)
      printf("SCHED_RDY\t");
    if(FIRE_RDY && op_code != FIRED)
      printf("FIRE_RDY\t");
    if(FIRE_WAIT)
      printf("FIRE_WAIT");
  }
  else
    printf("completed");
  printf("\n");

  return true;
}

bool proc_inst_t::reset(){
  op_code = NOT_USED;
  dest_tag = DISP = SCHED = EXEC = 0;
  dest_ready = src_ready[0] = src_ready[1] = false;
  FIRE_RDY = SCHED_RDY = FIRE_WAIT =  COMPLETED = false;
  return true;
}
////////////////_proc_inst_t/////////////
//////////////IF_t//////////////
bool IF_t::init(uint32_t len){
  Q.clear();
  for(uint16_t i=0; i<len;i++){
    proc_inst_t entry;
    Q.push_back(entry);
  }
  return true;
}


bool IF_t::print(){
  printf("*********the IF buffer is**********\n");
  //printf("i/addr/op/dest/src/src\n");
  for(uint16_t i=0; i<Q.size(); i++){
    Q[i].printOut();
  }
  return true;
}

bool IF_t::fetchInstr(){
  proc_inst_t inst;
  //uint16_t NUM_FETCH = fmin(model.fetch_rate,IF_BUF_LEN-Q.size());
  uint16_t NUM_FETCH = fmin(model.fetch_rate,numVacancyQ(model.DQ.Q));


  if(NUM_FETCH == 0)
    return true;

  if(model.allFetched)
    return false;

  //printf("fetching %u\n",NUM_FETCH);
  for(uint16_t i=0; i<NUM_FETCH; i++){
    if(read_instruction(&inst)){//fetched!
      inst.dest_tag = model.genTag();
      inst.FETCH = (model.clock).cycle;
      //inst.printOut();
      Q.push_back(inst);
    }

    else{
      model.allFetched = true;
      return false;
    }
  }
  
  return true;
}
//////////////IF_t//////////////
////////////DISP_t/////////
bool DISP_t::init(uint32_t len){
  Q.clear();
  for(uint32_t i=0; i<len; i++){
    proc_inst_t entry;
    Q.push_back(entry);
  }

  return true;
}

bool DISP_t::print(){
  //printf("*******the DQ is**********\n");
  //printf("i/addr/op/dest/src/src\n");
  for(uint16_t i=0; i<Q.size(); i++){
      Q[i].printOut();
  }
  return true;
}


bool DISP_t::loadFromIF(){
  uint16_t NUM_To_DISP = fmin(model.fetch_rate,numVacancyQ(model.DQ.Q));
  
  if(model.allDispatched)
    return false;

  uint16_t NUM_ELG = 0; //number of elligible to fetch in IF buffer
  for(uint16_t i=0; i<(model.IF).Q.size(); i++){
    if( Q[i].FETCH < model.clock.cycle)
      NUM_ELG ++;
    else
      break;
  }
  if(model.IF.Q.empty() && Q.empty()){
    model.allDispatched = true;
    return true;
  }

  NUM_To_DISP = fmin(NUM_ELG,NUM_To_DISP);
  //printf("dispatching %u\n",NUM_To_DISP);
  uint16_t pos; //free position
  bool hasVac;
  for(uint16_t i=0; i<NUM_To_DISP; i++){
    pos = firstVacancy(Q,hasVac);
    if(!hasVac)
      break;
    Q[pos] = (model.IF.Q[0]);
    Q[pos].DISP = model.clock.cycle;
    model.IF.Q.erase(model.IF.Q.begin());
    //Q[pos].printOut();
  }


  return true;
}



bool DISP_t::reserveSQ(){
  vector<uint64_t> insts;
  vector<uint16_t> index;
  vector<uint64_t> select_insts;
  vector<uint16_t> select_index;

  for(uint16_t i=0; i<Q.size(); i++){//the candidates to reserve in SQ; op_code labeled other than NOT_USED!
    if( Q[i].op_code != NOT_USED){
      insts.push_back(Q[i].dest_tag);
      index.push_back(i);
    }
  }


  uint16_t min_index;


  while(!insts.empty()){
    min_index = findMinIndex(insts);//position of smallest tag in insts
    select_insts.push_back(insts[min_index]);
    select_index.push_back(index[min_index]);
    insts.erase(insts.begin() + min_index);
    index.erase(index.begin() + min_index);
  }//sort the tags

  unsigned int FU,vac;
  bool hasVac;

  for(uint16_t i=0; i<select_index.size(); i++){
    FU = fmax(Q[select_index[i]].op_code,0);
    vac = firstVacancy(model.SQ[FU].Q,hasVac);
    if(!hasVac)//has no vacancy
      break;
    model.SQ[FU].Q[vac] = Q[select_index[i]];
    //model.SQ[FU].Q[vac].SCHED = (model.clock).cycle;//ready to move to the SQ by not actually move!this instr. is still in the DQ
    model.SQ[FU].Q[vac].dest_ready = false;
    //printf("r");
    //model.SQ[FU].Q[vac].printOut();
    
  }

  return true;
}


bool DISP_t::reserveSQdbg(){
  printf("debugging\n");
  vector<uint64_t> insts;
  vector<uint16_t> index;
  vector<uint64_t> select_insts;
  vector<uint16_t> select_index;

  for(uint16_t i=0; i<Q.size(); i++){//the candidates to reserve in SQ; op_code labeled other than NOT_USED!
    if( Q[i].op_code != NOT_USED){
      insts.push_back(Q[i].dest_tag);
      index.push_back(i);
    }
  }


  uint16_t min_index;
  for(uint16_t i=0; i<insts.size();i++)
    //    printf("\t%lu",insts[i]);
  printf("\n");
  
  for(uint16_t q=0;q<4;q++){
    if(insts.empty())
      break;
    min_index = findMinIndex(insts);//position of smallest tag in insts
    printf("%u\n",min_index);
    //select_insts.push_back(insts[min_index]);
    //select_index.push_back(index[min_index]);
    //insts.erase(insts.begin() + min_index);
    //index.erase(index.begin() + min_index);
  }//sort the tags


  return true;
}



bool DISP_t::visitRF(){
  int32_t SRC_REG;
  int32_t DEST_REG;
  for(uint16_t i=0; i<FU_TYPE; i++){
    for(uint16_t j=0; j<(model.SQ[i].Q).size(); j++){
      if((model.SQ[i]).Q[j].DISP <= model.clock.cycle && model.SQ[i].Q[j].op_code != NOT_USED && !model.SQ[i].Q[j].SCHED_RDY && !model.SQ[i].Q[j].FIRE_RDY){ //for each instr. scheduled in this cycle
	DEST_REG = model.SQ[i].Q[j].dest_reg;
	//printf("%lu's dest reg is %d\n",model.SQ[i].Q[j].dest_tag,DEST_REG);
	if(DEST_REG != NOP){	  
	  model.SQ[i].Q[j].SCHED_RDY = true;//ready to move to SQ
	  model.RF.Q[(uint16_t)DEST_REG].tag = model.SQ[i].Q[j].dest_tag;
	  model.RF.Q[(uint16_t)DEST_REG].ready = false;
	}

	else{//dest reg is -1
	  model.SQ[i].Q[j].SCHED_RDY = true;
	}


	for(uint16_t k=0; k<2; k++){//for both registers
	  SRC_REG = (model.SQ[i]).Q[j].src_reg[k];
	  if(SRC_REG == NOP){//src reg is -1
	    //	    printf("%lu's src %u reg is NOP\n",model.SQ[i].Q[j].dest_tag,k);
	    model.SQ[i].Q[j].src_ready[k] = true;
	    model.SQ[i].Q[j].src_tag[k] = 0;
	  }

	  else{//src reg other than -1, compare to the RF
	    uint16_t SRC_NUM = (uint16_t)SRC_REG;
	    //	    printf("%lu's src %u reg is %u\n",model.SQ[i].Q[j].dest_tag,k,SRC_REG);
	    if(model.RF.Q[SRC_NUM].ready){ //ready in RF
	      model.SQ[i].Q[j].src_ready[k] = true;
	      model.SQ[i].Q[j].src_tag[k] = 0;
	    }
	    else{//not ready in RF
	      model.SQ[i].Q[j].src_tag[k] = model.RF.Q[SRC_NUM].tag;
	      model.SQ[i].Q[j].src_ready[k] = false;
	    }
	  }

	}

	

      }
    }
  }
  return true;
}

bool DISP_t::printOut(){//for the retired in proc_t only
  //if(Q.size() < 10) //a.k.a how large the retired buffer is
  //  return false;
  
  vector<uint16_t> index;
  vector<uint64_t> tags;
  CDB_t newCDB;

  //printf("in cycle %lu\n", model.clock.cycle);
  for(uint16_t i=0;i<Q.size(); i++){
    //index.push_back(i);
    //tags.push_back(Q[i].dest_tag);
    //Q[i].printOutPlain();
    model.screen.Q.push_back(Q[i]);
  }

  //vector<uint16_t> CDB_dlist;
 

  /*
  uint16_t min_index;
  while(!index.empty()){
    min_index = findMinIndex(tags);
    printf("this is the instr.%lu\n", Q[index[min_index]].dest_tag);
    Q[index[min_index]].printOutPlain();
    index.erase(index.begin() + min_index);
    tags.erase(tags.begin() + min_index);
  }
  */


  uint16_t match;
  for( uint16_t i=0; i<model.CDB.Q.size(); i++){
    match = findQIndex(Q,model.CDB.Q[i].tag);
    if(match >= Q.size()){//not in the retired
      newCDB.Q.push_back(model.CDB.Q[i]);
    }
  }

  model.CDB = newCDB;
  stats.retired_instruction += Q.size();
  //printf("stats is now %lu\n",stats.retired_instruction);
  Q.clear();
  return true;
}
////////////DISP_t/////////
///////////RF_t////////
bool RF_t::init(){
  Q.clear();
  for(uint16_t i=0; i<REG_TOT; i++){
    entry3 entry(i,true,0);
    Q.push_back(entry);
  }

  return true;
}

bool RF_t::print(){
  printf("**********Register file is********** \n");
  for(uint16_t i=0; i<Q.size(); i++){
    if(Q[i].ready)
      continue;
    printf("%u/", Q[i].reg_num);
    if(Q[i].ready)
      printf("rdy/");
    else
      printf("not/");
    //printf("%lu\n",Q[i].tag);
  }

  return true;
}

///////////RF_t///////////
//////////entry3////////
entry3::entry3(uint16_t num, bool rdy, uint64_t tagging){
  reg_num = num;
  ready = rdy;
  tag = tagging;
  negative = false;
}

bool entry3::trans(const proc_inst_t &src){
  if(src.dest_reg >= 0){
    reg_num = (uint16_t)src.dest_reg;
    negative = false;
  }

  else{
    reg_num = 0;
    negative = true;
  }
  tag = src.dest_tag;
  ready = false;
  return true;
}


bool DISP_t::display(){
  uint16_t SCALE = 100;
  if(Q.size() < SCALE)
    return false;

  vector<uint64_t> tags;
  for(uint16_t i=0; i<Q.size();i++){
    tags.push_back(Q[i].dest_tag);
  }

  uint16_t min_index = findMinIndex(tags);
  uint64_t min_tag = tags[min_index];

  uint64_t sel_tag = min_tag;
  uint16_t sel_index;

  while(true){
    sel_index = findQIndex(Q,sel_tag);
    if(sel_index > Q.size())//not match!
      break;
    
    else{//match!
      Q[sel_index].printOutPlain();
    }

    sel_tag ++;

  }
  
  DISP_t newDISP;
  for(uint16_t i=0;i<Q.size(); i++){
    if( Q[i].dest_tag >= sel_tag)
      newDISP.Q.push_back(Q[i]);
  }
  
  Q = newDISP.Q;

  return true;
}


bool DISP_t::displayAll(){
  if(Q.empty())
    return 0;

  vector<uint64_t> tags;

  for(uint16_t i=0; i<Q.size(); i++){
    tags.push_back(Q[i].dest_tag);
  }
  
  uint64_t min_tag;
  uint16_t min_index,min_Q_index;

  while(!tags.empty()){
    min_index = findMinIndex(tags);
    min_tag = tags[min_index];
    min_Q_index = findQIndex(Q,min_tag);
    Q[min_Q_index].printOutPlain();
    tags.erase(tags.begin() + min_index);
  }
 
  Q.clear();

  return true;
}

//////////entry3////////
////////////SCHED_t//////
bool SCHED_t::init(uint32_t len){
  Q.clear();
  for(uint32_t i=0; i<len; i++){
    proc_inst_t entry;
    Q.push_back(entry);
  }
  return true;
}

bool SCHED_t::print(uint16_t FU){
  printf("*******the SQ.%u is**********\n",FU);
  //printf("i/addr/op/dest/src/src\n");
  for(uint16_t i=0; i<Q.size(); i++){
    if(Q[i].op_code != NOT_USED)
      Q[i].printOut();
  }
  return true;
}
////////////SCHED_t/////
////////SB_t////////
bool SB_t::init(uint16_t LAT,uint32_t FU_NUM){
  vector<entry3> FU;
  for(uint32_t i=0; i<FU_NUM; i++){
    FU.clear();
    for(uint16_t j=0; j<=LAT; j++){ //last is the buffer
      entry3 entry(NON_REG,true,NON_TAG);
      FU.push_back(entry);
    }
    Q.push_back(FU);
  }
  return true;
}

bool SB_t::print(uint16_t FU){
  printf("**********ScoreBoard.%d is********** \n",FU);
  if(Q.size() == 0){
    printf("empty\n");
    return false;
  }
  for(uint16_t i=0; i<Q.size(); i++){
    for(uint16_t j=0; j<Q[i].size(); j++){
      printf("%u/", Q[i][j].reg_num);
      if(Q[i][j].ready)
	printf("rdy/");
      else
	printf("not/");
      //      printf("%lu/",Q[i][j].tag);
      if(Q[i][j].negative)
	printf("neg\t");
      else
	printf("pos\t");
    }
    printf("\n");
  }

  return true;
}

///////SB_t/////////
////////////proc_t/////////
bool proc_t::printAll(){
  printf("The proc model have:\n");
  //  printf("\t\t\tclock is now at %lu \n", clock.cycle);
  //printf("\tTag to give is %lu\n",avail_tag+1);
  IF.print();
  if(!DQ.Q.empty()){
    printf("///////////DQ is////////////\n");
    DQ.print();
  }
  for(uint16_t i=0; i<FU_TYPE; i++){
    if(!SQ[i].Q.empty())
      SQ[i].print(i);
  }
  
  RF.print();
  CDB.print();
  for(uint16_t i=0; i<FU_TYPE; i++){
    SB[i].print(i);
  }
  printf("///////////RETIRED is///////\n");
  retired.print();
  return true;
}

uint64_t proc_t::genTag(){
  avail_tag ++;
  return avail_tag;

}


bool proc_t::SQmarkFire(){
  vector<uint64_t> waitlist;
  vector<uint16_t> FUlist;
  vector<uint16_t> indexlist;

  uint16_t min_index,FU,indi;
  //  uint64_t min_tag;

  for(uint16_t i=0; i<FU_TYPE; i++){
    for(uint16_t j=0; j<SQ[i].Q.size(); j++){
      if( SQ[i].Q[j].FIRE_WAIT ){
	if(SQ[i].Q[j].src_ready[0] && SQ[i].Q[j].src_ready[1]){
	  //SQ[i].Q[j].FIRE_RDY = true;
	  //SQ[i].Q[j].FIRE_WAIT = false;
	  waitlist.push_back(SQ[i].Q[j].dest_tag);
	  FUlist.push_back(i);
	  indexlist.push_back(j);
	}
      }
    }
  }
  

  while(!waitlist.empty()){
    min_index = findMinIndex(waitlist);
    //    min_tag = waitlist[min_index];
    indi = indexlist[min_index];
    FU = FUlist[min_index];
    for(uint16_t i=0; i<SB[FU].Q.size();i++){
      if( SB[FU].Q[i][0].ready && SB[FU].Q[i][0].reg_num == NON_REG ){
	SB[FU].Q[i][0].ready = false;
	SQ[FU].Q[indi].FIRE_RDY = true;
	SQ[FU].Q[indi].FIRE_WAIT = false;
	break;
      }
    }

    waitlist.erase(waitlist.begin()+min_index);
    FUlist.erase(FUlist.begin()+min_index);
    indexlist.erase(indexlist.begin()+min_index);
  }


  return true;
}


bool proc_t::fireSQ(){//actually fire the instr. in SQ
  for(uint16_t i=0; i<FU_TYPE; i++){
    for(uint16_t j=0; j<SQ[i].Q.size(); j++){
      if(SQ[i].Q[j].FIRE_RDY && SQ[i].Q[j].op_code != FIRED){//ready to fire!
	for(uint16_t p=0; p<SB[i].Q.size(); p++){//looking for an empty FU
	  if(SB[i].Q[p][0].reg_num == NON_REG){
	    SQ[i].Q[j].op_code = FIRED;
	    SB[i].Q[p][0].trans(SQ[i].Q[j]);
	    break;
	  }
	}
	SQ[i].Q[j].op_code = FIRED;
	SQ[i].Q[j].EXEC = model.clock.cycle;
	//printf("fired\n");
	//SQ[i].Q[j].printOut();
      }
    }
  }


  return true;
}


bool proc_t::execFU(){  
  for(uint16_t i=0; i<FU_TYPE; i++){//for each type of FU
    for( uint16_t j=0; j<SB[i].Q.size();j++){//for each ALU of that type
      SB[i].Q[j].pop_back();//throw the content of the buffer
      entry3 empty_entry(NON_REG,true,NON_TAG);
      SB[i].Q[j].insert(SB[i].Q[j].begin(),empty_entry);//mark the first vac as empty!
    }
  } 
  return true;
}


bool proc_t::updateSQbyCDB(){
  //uint64_t SRC_TAG;
  uint16_t match;

  for(uint16_t i=0; i<FU_TYPE; i++){
    for(uint16_t j=0; j<SQ[i].Q.size();j++){
      // if( SQ[i].Q[j].FIRE_WAIT){
      for(uint16_t k=0; k<2; k++){
	match = findQIndex(CDB.Q,SQ[i].Q[j].src_tag[k]);
	if(match < CDB.Q.size()){
	  SQ[i].Q[j].src_ready[k] = true;
	}
      }
      // }
    }
  }

  return true;
}


bool proc_t::stateUpdate(){
  
  return true;
}




bool proc_t::mvDQtoSQ(){
  vector<uint64_t> tags;

  for(uint16_t i=0; i<FU_TYPE;i++){
    for(uint16_t j=0; j<SQ[i].Q.size(); j++){
      if( SQ[i].Q[j].SCHED_RDY && !SQ[i].Q[j].FIRE_WAIT && !SQ[i].Q[j].FIRE_RDY){
	SQ[i].Q[j].SCHED = clock.cycle;
	SQ[i].Q[j].FIRE_WAIT = true;
	tags.push_back(SQ[i].Q[j].dest_tag);
      }      
    }
  }

  //uint16_t match;
  for(uint16_t i=0; i<DQ.Q.size(); i++){
    for(uint16_t j=0; j<tags.size(); j++){
      if( tags[j] == DQ.Q[i].dest_tag)
	DQ.Q[i].reset();
    }
  }
  
  return true;
}


bool proc_t::completeExecFU(){
  uint16_t last;

  for(uint16_t i=0; i<FU_TYPE;i++){
    for(uint16_t j=0; j<SB[i].Q.size(); j++){//for each FU of the type
      last = SB[i].Q[j].size() - 1;
      if(SB[i].Q[j][last].reg_num != NON_REG){
	CDB.Q.push_back(SB[i].Q[j][last]);
      }

    }

  }

  return true;
}


bool proc_t::RFwrtByCDB(){
  uint16_t REG;
  for(uint16_t i=0; i<CDB.Q.size(); i++){
    if(! CDB.Q[i].negative){// dest_reg is not negative
      REG = CDB.Q[i].reg_num;
      if( RF.Q[REG].tag == CDB.Q[i].tag){
	RF.Q[REG].ready = true;
	CDB.Q[i].ready = true;
      }
      else if( RF.Q[REG].tag > CDB.Q[i].tag)
	CDB.Q[i].ready = true;
      
    }
    else//if dest reg is -1
      CDB.Q[i].ready = true;
  }
  

  return true;
}


bool proc_t::RFwrtByCDBdbg(){
  uint16_t REG;
  for(uint16_t i=0; i<CDB.Q.size(); i++){
    if(! CDB.Q[i].negative){// dest_reg is not negative
      REG = CDB.Q[i].reg_num;
      if( RF.Q[REG].tag == CDB.Q[i].tag){
	RF.Q[REG].ready = true;
	CDB.Q[i].ready = true;
      }
      else if( RF.Q[REG].tag > CDB.Q[i].tag)
	CDB.Q[i].ready = true;
      
    }
    else//if dest reg is -1
      CDB.Q[i].ready = true;
  }
  

  return true;
}

bool proc_t::DQvisitRF(){
  vector<uint64_t> tags;
  for(uint16_t i=0; i<FU_TYPE; i++){
    for(uint16_t j=0; j<SQ[i].Q.size();j++){
      if(!SQ[i].Q[j].SCHED_RDY && SQ[i].Q[j].op_code != NOT_USED){
	tags.push_back(SQ[i].Q[j].dest_tag);
      }
    }
  }

  uint16_t min_index;
  uint64_t min_tag;
  int32_t SRC_REG,DEST_REG;
  uint16_t SRC;
  uint16_t DEST;
  while(!tags.empty()){
    min_index = findMinIndex(tags);
    min_tag = tags[min_index];
    for(uint16_t i=0; i<FU_TYPE; i++){
      for(uint16_t j=0; j<SQ[i].Q.size();j++){//search for every position in SQ
	if( SQ[i].Q[j].dest_tag == min_tag){//the matching tag
	  SQ[i].Q[j].SCHED_RDY = true;
	  for(uint16_t m=0; m<2; m++){//for both src reg
	    SRC_REG = SQ[i].Q[j].src_reg[m];
	    if(SRC_REG < 0)
	      SQ[i].Q[j].src_ready[m] = true;
	    else{
	      SRC = (uint16_t)SRC_REG;
	      if(RF.Q[SRC].ready)
		SQ[i].Q[j].src_ready[m] = true;
	      else{
		SQ[i].Q[j].src_ready[m] = false;
		SQ[i].Q[j].src_tag[m] = RF.Q[SRC].tag;
	      }	    
	    }
	  }
	  DEST_REG = SQ[i].Q[j].dest_reg;
	  if(DEST_REG >= 0){
	    DEST = (uint16_t) DEST_REG;
	    RF.Q[DEST].tag = SQ[i].Q[j].dest_tag;
	    RF.Q[DEST].ready = false;
	  }
	}
      }
    }
    tags.erase(tags.begin() + min_index);
  }
  

  return true;
}


bool proc_t::SQdltCompleted(){
  uint16_t match;
  for(uint16_t j=0; j<CDB.Q.size(); j++){
    for(uint16_t i=0; i<FU_TYPE; i++){
      match = findQIndex(SQ[i].Q,CDB.Q[j].tag);
      if(match < SQ[i].Q.size() && CDB.Q[j].ready){
	SQ[i].Q[match].STATE = clock.cycle ;
	retired.Q.push_back(SQ[i].Q[match]);
	SQ[i].Q[match].reset();
      }
    }
  }

  return true;
}

bool proc_t::printRetired(){


  return true;
}
/////////proc_t///////////
///////////my_clock_t//////
my_clock_t::my_clock_t(){
  inHalf1 = true;
  cycle = 0;
}

bool my_clock_t::tick(){
  if(inHalf1)
    inHalf1 = false;

  else{
    inHalf1 = true;
    cycle ++;
  }
  return true;
}


///////my_clock_t////////
//////CDB_t///////
bool CDB_t::print(){
  printf("**********CDB is********** \n");
  if(Q.size() == 0){
    printf("empty\n");
    return false;
  }
  for(uint16_t i=0; i<Q.size(); i++){
    printf("%u/", Q[i].reg_num);
    if(Q[i].ready)
      printf("rdy/");
    else
      printf("not/");
    //    printf("%lu/",Q[i].tag);
    if(Q[i].negative)
      printf("neg\n");
    else
      printf("pos\n");
  }

  return true;
}

bool proc_t::bypassSQ(const entry3 &result){

  for(uint16_t i=0; i<FU_TYPE; i++){
    for(uint16_t j=0; j<(model.SQ[i]).Q.size(); j++){
      if( result.tag == model.SQ[i].Q[j].dest_tag ){
	model.SQ[i].Q[j].STATE = model.clock.cycle;	
	model.retired.Q.push_back(model.SQ[i].Q[j]);
	//printf("^");
	//model.SQ[i].Q[j].printOut();
	model.SQ[i].Q[j].reset();
      }
    }
  }

  
  return true;
}

//////CDB_t/////////




bool read_instruction(proc_inst_t* p_inst)
{
  int ret;
    
  if (p_inst == NULL)
    {
      fprintf(stderr, "Fetch requires a valid pointer to populate\n");
      return false;
    }
    
  ret = fscanf(stdin, "%x %d %d %d %d\n", &p_inst->instruction_address,
	       &p_inst->op_code, &p_inst->dest_reg, &p_inst->src_reg[0], &p_inst->src_reg[1]); 
  if (ret != 5) {
    return false;
  }
    
  return true;
}



uint16_t numVacancyQ(const vector<proc_inst_t> &que){
  uint16_t ret = 0;
  for(uint16_t i=0; i<que.size(); i++){
    if( que[i].op_code == NOT_USED)
      ret ++;
  }  
  return ret;
}


uint16_t firstVacancy(const vector<proc_inst_t> &que,bool &has){
  has = true;

  for(uint16_t i=0; i<que.size(); i++){
    if(que[i].op_code == NOT_USED)      
      return i;
  }

  has = false;
  return 0;

}

uint16_t findMinIndex(const vector<uint64_t> &tags){
  uint64_t min ;
  
  if(tags.empty())
    return 101;

  else if (tags.size() == 1)
    return 0;

  min = tags[0];
  
  for(uint16_t i=0; i<tags.size(); i++){
    if(tags[i] < min)
      min = tags[i];
  }
  

  //min tag is found, return its index in the vector
  for(uint16_t i=0; i<tags.size(); i++){
    if(tags[i] == min)
      return i;
  }
  
  return 101;
}

uint16_t findQIndex(const vector<entry3> & entries,uint64_t tagging){
  for(uint16_t i=0; i<entries.size(); i++){
    if(entries[i].tag == tagging)
      return i;
  }

  return entries.size() + 1;
  
}



uint16_t findQIndex(const vector<proc_inst_t>& entries,uint64_t tagging){
  for(uint16_t i=0; i<entries.size(); i++){
    if(entries[i].dest_tag == tagging)
      return i;

  }

  return entries.size() + 1;

  return true;
}


