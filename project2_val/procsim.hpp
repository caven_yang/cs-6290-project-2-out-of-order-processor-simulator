#ifndef PROCSIM_HPP
#define PROCSIM_HPP

#include <cmath>
#include <cstdint>
#include <cinttypes>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <unistd.h>

using namespace std;

#define DEFAULT_K0 1
#define DEFAULT_K1 2
#define DEFAULT_K2 3

#define DEFAULT_M 2
#define DEFAULT_F 4
#define DEFAULT_D 4

#define FU_TYPE 3
#define INIT_TAG 0
#define REG_TOT 32

#define NOT_USED -3//used in op_code only!
#define FIRED -5 //used in op_code in SQ only!
#define NON_TAG 0 //used in entry3
#define NOP -1 // used as op_code or src/dest reg
#define NON_REG 129//used in SB

#define IF_BUF_LEN 20
#define RETIRE_SIZE 40//size of the retired buffer; 

//this is to have the result printed out in order

const uint16_t LAT_FU[] = {1,2,3};

typedef struct _proc_inst_t//used as entry for IF buffer, DQ and SQ
{
  uint32_t instruction_address;
  int32_t op_code;
  int32_t src_reg[2];
  int32_t dest_reg;
  bool src_ready[2];
  bool dest_ready;//obsolete term
  uint64_t dest_tag;
  uint64_t src_tag[2];
    // You may introduce other fields as needed
  uint64_t FETCH,DISP,SCHED,EXEC,STATE;//record when this instr. is fetched/dispatched etc.
  bool SCHED_RDY,FIRE_RDY,FIRE_WAIT,COMPLETED;//mark if an instr. is ready to schedule/ready to fire/pending to be marked as ready to fire/is completed; note that another status is marked by the op_code: FIRED = -5 means an instr. is fired and is waiting for its result value on the CDB and NOT_USED = -3 means this is a vacant entry
  _proc_inst_t();
  bool printOutPlain();//used for print this entry in a plain flavor, used in model.screen.display(),which issues the final results in the screen
  bool printOut();//used for debugging; print all the info contained in this entry
  bool reset();//when an instr. is completed in SQ, or when it is moved from DQ to SQ, this function is called to mark this entry as NOT_USED and cleaning all the other stats: i.e. dest_tag, FETCH,  FIRE_RDY etc.
} proc_inst_t;

typedef struct _proc_stats_t
{
  float avg_inst_fire;
  unsigned long retired_instruction;
  unsigned long cycle_count;
  float avg_ipc;
  float perc_branch_pred;  
} proc_stats_t;

struct my_clock_t{
  uint64_t cycle;
  bool inHalf1;//true if this is in the first half of the cycle
  my_clock_t();
  bool tick();//if in the first half of a cycle, I will send the clock to the other half of the same cycle; if in the second half, I will send the clock to the next cycle and mark it as in the first half
};

bool read_instruction(proc_inst_t* p_inst);

void setup_proc(uint64_t d, uint64_t k0, uint64_t k1, uint64_t k2, uint64_t f, uint64_t m);
void run_proc(proc_stats_t* p_stats);
void complete_proc(proc_stats_t* p_stats);

struct entry3{ //entries for CDB,RF and SB; entry3 means this is an object 3 in 1; when used as each entry, different parts of the data is utilized
  uint16_t reg_num;//not used in RF
  bool ready;
  uint64_t tag;
  bool negative;//used in SB, if an instr. pushed to SB has a negative dest reg, then this is set to true
  entry3(uint16_t num, bool rdy, uint64_t tagging);
  bool trans(const proc_inst_t &src);//used when an instr. is pushed from SQ to SB; note that the entry in SQ won't be deleted immediately but after the SQ captures the finished value on CDB
};

struct IF_t{//the IF buffer type
  vector<proc_inst_t> Q;
  bool init(uint32_t len);
  bool print();//used for debugging
  bool fetchInstr();//return false on completing fetching all instructions;fetch the instr. from trace file to IF buffer
};

struct DISP_t{//the DQ data type; also used in proc_t.retired and proc_t.screen
  vector<proc_inst_t> Q;
  bool init(uint32_t len);
  bool print();
  bool loadFromIF();
  bool reserveSQ();//reserve the SQ, used as DQ only
  bool pushToScreen(); //used for retired only; 
  bool display(); //for proc_t.screen only!
  bool displayAll(); //display whatever is in proc_t.screen in order
};

struct SCHED_t{//the SQ data type; the model has 3 in this case
  vector<proc_inst_t> Q;
  bool init(uint32_t len);//initialize at the beginning of the program
  bool print(uint16_t FU);//used for debugging, print all the info about the SQ
};

struct RF_t{//the reg file data type
  vector<entry3> Q;
  bool init();//initialize
  bool print();//used for debugging
};

struct CDB_t{//the CDB data type
  vector<entry3> Q;
  bool print();//used for debugging
};

struct SB_t{//the SB data type, the model has 3 in this case
  vector<vector<entry3> > Q;
  bool init(uint16_t LAT,uint32_t FU_NUM);
  bool print(uint16_t FU);
};


struct proc_t{
  uint16_t fetch_rate;
  my_clock_t clock;
  uint64_t avail_tag;//tags to assign, kept so that all tags are different
  bool allFetched,allDispatched;//if all the instr. are fetched? or all are dispatched? helps some functions to run faster
  IF_t IF; //IF buffer
  DISP_t DQ;//dispatch queue
  SCHED_t SQ[FU_TYPE]; //schedule queue
  RF_t RF;//reg file
  CDB_t CDB;//comman data bus
  SB_t SB[FU_TYPE];//scoreboard
  DISP_t retired;//retired instr.
  DISP_t screen;//a.k.a reorder buffer: make sure the completed instr. are printed in order
  bool printAll();//print out all the entries of the procsim:used for debugging
  uint64_t genTag();//generate a new tag by incrementing the last tag generated by one
  bool SQmarkFire();//mark a ready entry in the SQ as READY to fire
  bool execFU();//execute the FUs in the procsim
  bool updateSQbyCDB();//update the SQ by the CDB
  bool mvDQtoSQ();//actually move an instr from SQ to the correct SQ posistion!
  bool fireSQ();//fire the instr. in the SQ that are ready!can be executed in the same cycle
  bool completeExecFU();//completing the instr. and pushing them to CDB
  bool RFwrtByCDB();//reg file updated by CDB
  bool DQvisitRF();//DQ unit reads the reg file and update the reserved slots in SQ
  bool SQdltCompleted();//SQ unit deletes the completed instr. from SQ
};


bool read_instruction(proc_inst_t* p_inst);

uint16_t numVacancyQ(const vector<proc_inst_t> &que);//return the number of vacant entries;
uint16_t firstVacancy(const vector<proc_inst_t> &que,bool &has);//return the index of the vacant entry in the que; has is set to false if no vacancy in que;
uint16_t findMinIndex(const vector<uint64_t> &tags); //find the smallest tag indexed by the vector!don't op on am empty vector

uint16_t findQIndex(const vector<entry3> & entries,uint64_t tagging);//return entries.size()+1 if the tagging is not found
uint16_t findQIndex(const vector<proc_inst_t>& entries,uint64_t tagging);//the twin function but works on a different data type



extern proc_t model;
extern proc_stats_t stats;


#endif /* PROCSIM_HPP */

