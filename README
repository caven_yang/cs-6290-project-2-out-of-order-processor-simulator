Project Description:

In this project, you will construct a simulator for an out-of-order superscalar processor that uses the Tomasulo algorithm and fetches N instructions per cycle. Then you will use the simulator to find the appropriate number of function units and fetch rate for each benchmark.
Specification of simulator:
1. INPUT FORMAT

The input traces will be given in the form:

<address> <function unit type> <dest reg #> <src1 reg #> <src2 reg#>
<address> <function unit type> <dest reg #> <src1 reg #> <src2 reg#>
...

Where

<address> is the address of the instruction (in hex)
<function unit type> is either “-1”, "0", "1" or "2"
<dest reg #>, <src1 reg#> and <src2 reg #> are integers in the range [0..31]
note: if any reg #  is -1, then there is no register for that part of the instruction (e.g., a branch instruction has -1 for its <dest reg #>).  The instruction may still have immediate values or other useful work that is not pertinent for this project.

For example:

ab120024 2 1 2 3
ab120028 1 4 1 3
ab12002c -1 -1 4 7
bc213130 0 0 1 0

Means:

"operation type 2"  R1, R2, R3
"operation type 1"  R4, R1, R3
"operation type -1"  -, R4, R7       branch and no destination register!
"operation type 0" R0, R1, R0            Destination is also a source

Command-line parameters

Your project should include a Makefile which builds binary in your project's root directory named proc_sim.  The program should run from this root directory as:

./proc_sim –d D –f F –m M –j J –k K –l L -i trace_file

The command line parameters are as follows:

•     F – Fetch rate (instructions per cycle)

•     D – Dispatch queue multiplier

•     M – Schedule queue multiplier

•     J – Number of k0 FUs

•     K – Number of k1 FUs

•     L – Number of k2 FUs

•     trace_file – Path name to the trace file

Traces will be placed on the website.
2. FUNCTION UNIT MIX
 

Function unit type
	

Number of units*
	

Latency

0
	

parameter: k0
	

1

1
	

parameter: k1
	

2

2
	

parameter: k2
	

3

Notes:

    Instructions of type -1 are branches (see below) and are executed in the type 0 functional units.
    The number of function units is a parameter of the simulation and should be adjustable along the range of 1 to 3 units each (see experiments below).
    Function units of types 1 and 2 are pipelined into stages. (2 stages for function unit type 1; 3 stages for function unit type 2).

3. PIPELINE TIMING

Assume the following pipeline structure:
 

Stage
	

Name
	

Number of cycles per instruction

1
	

Instruction Fetch/Decode
	

1

2
	

Dispatch
	

Variable, depends on resource conflicts

3
	

Scheduling
	

Variable, depends on data dependencies

4
	

Execute
	

Depends on function unit type, see table above

5
	

State update
	

Variable, depends on data dependencies (see notes 10 and 11 below)

Details:

    You should explicitly model the dispatch and scheduling queues
    The dispatch queue has d*(m*k0 + m*k1+m*k2) entries.
    The scheduling queue is divided into three sub-queues.  There is one sub-queue each for operations of type 0, 1, or 2.  The queue for type 0 operations has m*k0 entries, the queue for type 1 operations has m*k1 entries, and the queue for type 2 operations has m*k2 entries (there are k0 type 0 function units, k1 type 1 function units and k2 type 2 function units). Any function unit can receive instructions from any scheduling queue entry for its type.
    If there are multiple independent instructions ready to fire during the same cycle in the scheduling queue, service them in tag order (i.e., lowest tag value to highest). (This will guarantee that your results can match ours.)
    A fired instruction remains in the scheduling queue until it completes.
    When the dispatch queue is full, stage 1 (Fetch/Decode) should be stalled until the cycle following when space is available in the dispatch queue.
    When the scheduling queue is full, stage 2 (Dispatch) should be stalled until the cycle following when space is available in the scheduling queue.
    The dispatch rate is limited by the capacity of the scheduling unit.
    The fire rate (issue rate) is only limited by the number of available FUs.
    There are unlimited result buses (also called Common Data Buses, or CDBs).
    Assume that instructions retire in the same order that they complete. Instruction retirement is unconstrained (imprecise interrupts are possible).

3.1 FETCH/DECODE UNIT

The fetch/decode rate is N instructions per cycle. Each cycle, the Fetch/Decode unit can always supply N instructions to the Dispatch unit, provided that there is room for these instructions in the dispatch queue.
3.2 WHEN TO UPDATE THE CLOCK

Note that the actual hardware has the following structure:

    Instruction Fetch/Decode
    PIPELINE REGISTER
    Dispatch
    PIPELINE REGISTER
    Scheduling
    PIPELINE REGISTER
    Execute
    PIPELINE REGISTER
    (Execute)  // types 2 and 3
    (PIPELINE REGISTER)
    (Execute)  // type 3
    (PIPELINE REGISTER)
    State update

Instruction movement only happens when the latches are clocked, which occurs at the rising edge of each clock cycle. You must simulate the same behavior of the pipeline latches, even if you do not model the actual latches. For example, if an instruction is ready to move from scheduling to execute, the motion only takes effect at the beginning of the next clock cycle.

Note that the latching is not strict between the dispatch unit and the scheduling unit, since both units use the scheduling queues. For example, if the dispatch unit inserts an instruction into one of the scheduling queues during clock cycle J, that instruction must spend at least cycle J+1 in the scheduling unit.

In addition, assume each clock cycle is divided into two half cycles (you do not need to explicitly model this, but please make sure your simulator follows this ordering of events):
 

Cycle half
	

Action

first half
	

The register file is written via a result bus

 
	

Any independent instruction in scheduling queue is marked to fire

 
	

The dispatch unit reserves slots in the scheduling queues

 
	

Instructions complete execution

second half
	

The register file is read by Dispatch

 
	

Scheduling queues are updated via a result bus

 
	

The state update unit deletes completed instructions from scheduling queue
3.3 OPERATION OF THE DISPATCH QUEUE

Note that the dispatch queue is scanned from head to tail (in program order).   When an instruction is inserted into the scheduling queue, it is deleted from the dispatch queue.
3.4 TAG GENERATION

Tags are generated sequentially for every instruction, beginning with 0. The first instruction in a trace will use a tag value of 0, the second will use 1, etc. The traces are sufficiently short so that you should not need to reuse tags.
3.5 BUS ARBITRATION

Since there are an unlimited number of result buses (common data buses), all instructions that complete in the same cycle can update the schedule queues and register file in the following cycle. Unless we agree on some ordering of this updating, multiple different (and valid) machine states can result.  This will complicate validation considerably.  Therefore, assume the order of update is the same as the order of the tag values.

Example:

Tag values of instructions waiting to complete: 100, 102, 110, 104

Action:

The instruction with the tag value = 100 updates the schedule queue/register file, then instruction 102, then 104, then 110, all of these happen in parallel, at the same time, and at the beginning of the next cycle!

3.6 FLOW CONTROL

When the dispatch queue is full, instruction fetch/decode halts. In other words:

If (dispatch queue is full) then {

Do not fetch/decode

}

else {

Fetch/decode up to N instructions from the trace into the dispatch queue (as many instructions as there are room for in the dispatch queue, up to N total)

}
3.7 INSTRUCTION OUTPUT
The simulator outputs cycle information for each instruction.  With the first instruction numbered 1 and the first cycle being 0, output the cycle when every instruction enters each pipeline unit.
3.8 STATISTICS (OUTPUT)

The simulator outputs the following statistics after completion of the experimental run:

    Average number of instructions fired per cycle
    Total number of instructions in the trace
    Total simulated run time for the input: the run time is the total number of cycles from when the first instruction entered the instruction fetch/decode unit until when the last instruction completed.

4. EXPERIMENTS
4.1 VALIDATION

Your simulator must match the validation output that we will place on-line.
4.2 RUNS

For each trace on T-Square, use your simulator as follows:

    Vary the following parameters:
        Number of FUs of each type (k0, k1, k2) of either 1 or 2 or 3 units each.
        Schedule queue sizes (m*k0+m*k1+m*k2), where m = 2 or 4 or 8
        Dispatch Queue sizes d * (m*k0+m*k1+m*k2), where d = 1, 2 or 4
        Fetch rate, N = 2 or 4 or 8
    Based on the results, select the least amount of hardware (as measured by total number of FUs (k0+k1+k2), queue entries (m*k0+m*k1+m*k2), and dispatch queue size) for each benchmark trace that provides nearly the highest value for retired IPC. As with project 1, justify your design choice.

4.3 What to turn in

    The experimental results as described above. (As before, there are multiple answers for each trace file, so I will know which students "collaborated" inappropriately!)
    The commented source code for the simulator program, along with Makefile
        Tar.gz, tar.bz2, and zip are acceptable compressed file formats.
        The program will be run via the provided framework in order to validate its functionality.
        The experimental configurations may also be validated.
